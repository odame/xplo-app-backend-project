﻿"""
Definition of urls for XploAppBackend.
"""

from django.conf.urls import url, include
from django.contrib.auth.views import *

# Uncomment the next lines to enable the admin:

# from django.contrib import admin
# admin.autodiscover()

urlpatterns = [
    # Examples:
    # url(r'^$', views.home, name='home'),
    # url(r'^contact$', views.contact, name='contact'),
    # url(r'^about', views.about, name='about'),
    # url(r'^login/$',
    #    login,
    #    {
    #        'template_name': 'app/login.html',
    #        'authentication_form': BootstrapAuthenticationForm,
    #        'extra_context':
    #        {
    #            'title':'Log in',
    #            'year':datetime.now().year,
    #        }
    #    },
    #    name='login'),
    # url(r'^logout$',
    #    logout,
    #    {
    #        'next_page': '/',
    #    },
    #    name='logout'),

    url(r'^api/', include('api.urls')),
    url(r'^admin/', include('admin.urls'))

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
]
