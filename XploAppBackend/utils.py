from imgurpython import ImgurClient

import shutil
import imghdr
import os
import tempfile


allowed_file_types = ['jpeg', 'png', 'jpg'] 

def upload_image_to_imgur(file):
    # file_extension = "jpg"
    file_extension = imghdr.what("", h=file.read(1024 * 5))    
    if file_extension not in allowed_file_types:
        raise ValueError('File type is not allowed')
    if file_extension == 'jpeg':
        file_extension = 'jpg'
    file.seek(0)
    tmp = tempfile.NamedTemporaryFile(suffix=".{ext}".format(ext=file_extension), delete=False)
    shutil.copyfileobj(file, tmp)
    tmp.close()
    id = os.getenv('IMGUR_CLIENT_ID', '470d722327213c6')
    secret = os.getenv('IMGUR_CLIENT_SECRET', 'f943af8bcac68168ce6e6fbf50a377457858592f')
    imgur_client = ImgurClient(client_id=id, client_secret=secret)    
    imgur_response = imgur_client.upload_from_path(tmp.name)
    image_url = "https://i.imgur.com/{id}.{ext}".format(id=imgur_response['id'], ext=file_extension)
    # On Heroku, the file will be deleted after the response has been sent.
    # So we don't worry ourselves to delete it
    # os.unlink(tmp.name)
    return image_url
