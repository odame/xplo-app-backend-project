﻿from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework import serializers
from django.contrib.auth import authenticate
from django.contrib.auth.models import User

from api.models import *
import api
from api.utils import password_regex

import string


class AdminSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'email', 'first_name', 'last_name',)
        extra_kwargs = {'password':{'write_only':True},}

    def validate_password(self, value):
        if not password_regex.match(value):
            raise serializers.ValidationError("Password must be 8 or more alphanumeric characters")
        return value

    def create(self, validated_data):        
        user = User.objects.create_user(**validated_data)        
        return user

    def update(self, instance, validated_data):       
        instance.username = validated_data.get('username', instance.username)
        instance.email = validated_data.get('email', instance.email)
        instance.firstname = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.save()          
        return instance
        

class MemberSerializer(serializers.ModelSerializer):    
    photo_thumb = serializers.SerializerMethodField(required=False)  
    full_name = serializers.SerializerMethodField(required=False)

    class Meta:
        model = Member   
        read_only_fields = ['is_active', 'is_unregistered', 'date_registered', 'last_updated']     
    
    def get_photo_thumb(self, obj):
        """
        Returns the thumbnail url of the member's photo if he/she has
        """ 
        if hasattr(obj, 'photo') and obj.photo:
            return obj.photo[:-4] + 't' + obj.photo[-4:]
        else:
            return None
    
    def get_full_name(self, obj):
        return obj.full_name()
        return obj.first_name + ' ' + obj.last_name


class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        read_only_fields = ('date_created', 'date_sent',)
        message = serializers.CharField(max_length=300)    


class SectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Section        


class HostelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hostel        


class HostelInfoSerializer(serializers.ModelSerializer):
    section = SectionSerializer()
    class Meta:
        model = Hostel
        read_only_fields = ['section']


class CommitteeSerializer(serializers.ModelSerializer):
    logo = serializers.CharField(max_length=40, required=False)    
    class Meta:
        model = Committee
        fields = '__all__'
        read_only_fields = ['logo']


class ProgrammeOfStudySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProgrammeOfStudy


class MemberInfoSerializer(MemberSerializer):
    hostel = HostelInfoSerializer()
    committees = CommitteeSerializer(many=True)
    programme_of_study = ProgrammeOfStudySerializer()    


class AcademicYearSerializer(serializers.ModelSerializer):
    year_representation = serializers.SerializerMethodField()
    
    def get_year_representation(self, obj):
        return obj.get_year_str()

    class Meta:
        model = AcademicYear
        fields = ('id', 'start_year','end_year', 'year_representation')    


class ExecutivePositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExecutivePosition


class ExecutiveMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExecutiveMember


class ExecutiveMemberInfoSerializer(serializers.ModelSerializer):
    class ExecutiveSerializer(ExecutiveMemberSerializer):
        member = MemberInfoSerializer()
        position = ExecutivePositionSerializer()        

    executive_members = ExecutiveSerializer(many=True)

    class Meta:
        model = AcademicYear
        fields = ('id', 'year', 'executive_members',)


class WelcomeScreenImagesSerializer(serializers.ModelSerializer):    
    class Meta:
        model = WelcomeScreenImages


class FCMClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = FCMClient
        read_only_fields = ('notifications',)


class MusicFileUploadSerializer(serializers.Serializer):
    file = serializers.FileField()    
    class Meta:
        model = MusicFile
        fields = (
            # 'title', 
            'file', 'album',)


class MusicFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = MusicFile
        fields = ('id', 'title', 'url', 'date_uploaded', 'album')
        

class MusicAlbumSerializer(serializers.ModelSerializer):
    music_files = MusicFileSerializer(many=True, read_only=True)
    class Meta:
        model = MusicAlbum
        fields = ('title', 'date_created', 'music_files')
        read_only_fields = ('music_files', 'date_created',)