# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-12-27 09:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_committee_info'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fcmclient',
            name='client_id',
            field=models.TextField(primary_key=True, serialize=False),
        ),
    ]
