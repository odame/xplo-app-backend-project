﻿from api.views.academic_year import AcademicYearListCreate, AcademicYearUpdate
from api.views.authentication import Login, Logout
from api.views.committees import CommitteeListCreate, CommitteeUpdateDestroy
from api.views.executives import ExecutivePositionListCreate, ExecutiveMemberListCreate, ExecutiveMemberUpdateDestroy, ExecutivePositionUpdateDestroy
from api.views.executives import ExecutivePositionUpdateDestroy
from api.views.hostels import HostelListCreate, HostelUpdateDestroy
from api.views.members import MemberListCreate, MemberUpdateDestroy
from api.views.notifications import NotificationListCreate, NotificationUpdateDestroy, FCMClientCreate
from api.views.sections import SectionListCreate, SectionUpdateDestroy
from api.views.welcome_screen import WelcomeScreenImagesListCreate
from api.views.programme_of_study import ProgrammeOfStudyListCreate, ProgrammeOfStudyUpdateDestroy
from api.views.music import MusicFileUploadAPIView, MusicAlbumListCreateAPIView
from api.views import UploadPhotoFile

from django.conf.urls import url


urlpatterns = [    
    url(r'^members/$', MemberListCreate.as_view(), name='api-members'),
    url(r'^members/(?P<pk>[0-9]+)/$', MemberUpdateDestroy.as_view(), name='api-member_pk'),    

    url(r'^notifications/$', NotificationListCreate.as_view(), name='api-notifications'),
    url(r'^notifications/(?P<pk>[0-9]+)/$', NotificationUpdateDestroy.as_view(), name='api-notifications_pk'),
    url(r'^firebase-instance-id/new/$', FCMClientCreate.as_view(), name='new-firebase-instance-id'),

    url(r'^sections/$', SectionListCreate.as_view(), name='api-sections'),
    url(r'^sections/(?P<pk>[0-9]+)/$', SectionUpdateDestroy.as_view(), name='api-sections_pk'),

    url(r'^hostels/$', HostelListCreate.as_view(), name='api-hostels'),
    url(r'^hostels/(?P<pk>[0-9]+)/$', HostelUpdateDestroy.as_view(), name='api-hostel_pk'),

    url(r'^committees/$', CommitteeListCreate.as_view(), name='api-committees'),
    url(r'^committees/(?P<pk>[0-9]+)/$', CommitteeUpdateDestroy.as_view(), name='api-committees_pk'),

    url(r'^academic-years/$', AcademicYearListCreate.as_view(), name='api-academic_years'),
    url(r'^academic-years/(?P<pk>[0-9]+)/$', AcademicYearUpdate.as_view(), name='api-academic_years_pk'),

    url(r'^programmes-of-study/$', ProgrammeOfStudyListCreate.as_view(), name='api-programme_of_study'),
    url(r'^programmes-of-study/(?P<pk>[0-9]+)/$', ProgrammeOfStudyUpdateDestroy.as_view(), name='api-programme_of_study_pk'),

    url(r'^executive-positions/$', ExecutivePositionListCreate.as_view(), name='api-executive_positions'),
    url(r'^executive-positions/(?P<pk>[0-9]+)/$', ExecutivePositionUpdateDestroy.as_view(), name='api-executive_positions_pk'),

    url(r'^executive-members/$', ExecutiveMemberListCreate.as_view(),name='api-executive_members'),
    url(r'^executive-members/(?P<pk>[0-9]+)/$', ExecutiveMemberUpdateDestroy.as_view(), name='api-executive_members_pk'),

     url(r'^music-room/upload$', MusicFileUploadAPIView.as_view(), name='api-music_upload'),
     url(r'^music-room/album$', MusicAlbumListCreateAPIView.as_view(), name='api-music_album'),
    
    url(r'^welcome-screen-images/$', WelcomeScreenImagesListCreate.as_view(), name='api-welcome_screen_images'),

    url(r'^login/$', Login.as_view(), name='api-login'),
    url(r'^logout/$', Logout.as_view(), name='api-logout'),

    url(r'^upload-photo/$', UploadPhotoFile.as_view(), name='api-upload_photo'),
]