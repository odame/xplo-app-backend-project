﻿from api.models import Notification, FCMClient
import re

password_regex = re.compile(r'^(?=.*?\d)(?=.*?[a-zA-Z])[A-Za-z\d]{8,}$')


class NotificationHandler(object):
    """
    handles all notifications related actions
    """
    def send_notification(Notification):
        """
        send notification to members/users
        """
        pass