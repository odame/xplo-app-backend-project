from api.permissions import IsAdminOrReadOnly, IsAdminUser
from api.models import Hostel
from api.serializers import HostelSerializer, HostelInfoSerializer

from rest_framework import generics

from django.db.models import Prefetch


class HostelListCreate(generics.ListCreateAPIView):
    queryset = Hostel.objects.filter(is_active=True).prefetch_related('section')      
    permission_classes = (IsAdminOrReadOnly, )    

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return HostelInfoSerializer
        else:
            return HostelSerializer

    def list(self, request, *args, **kwargs):
        response = super(HostelListCreate, self).list(request,*args, **kwargs)
        if response.status_code == 200:
            response.data = {'data': response.data}
        return response   


class HostelUpdateDestroy(generics.UpdateAPIView, generics.DestroyAPIView):
    queryset = Hostel.objects.filter(is_active=True)
    permission_classes = (IsAdminUser, )
    serializer_class = HostelSerializer
