from api.permissions import IsAdminOrReadOnly, IsAdminUser
from api.models import Member, Hostel
from api.serializers import MemberSerializer, MemberInfoSerializer

from django.db.models import Q, Prefetch
from django.db import transaction

from rest_framework import generics
from rest_framework.response import Response


class MemberListCreate(generics.ListCreateAPIView):    
    # serializer_class = MemberSerializer
    permission_classes = (IsAdminOrReadOnly,)
    # we deal with only members who are currently active
    queryset = Member.objects.filter(is_active=True).filter(is_unregistered=False).prefetch_related(
        Prefetch(
            'hostel',
            queryset=Hostel.objects.all().select_related('section')
        )
    )    

    # def filter_queryset(self, queryset):
    #     if 'search' in self.request.query_params:
    #         search = self.request.query_params['search']
    #         first_name_filter = Q(first_name__icontains=search)
    #         last_name_filter = Q(last_name__icontains=search)
    #         email_filter = Q(email_name__icontains=search)
    #         queryset = queryset.filter(first_name_filter | last_name_filter | email_filter)
    #     return super(MemberListCreate, self).filter_queryset(queryset) 
         

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return MemberInfoSerializer
        else:
            return MemberSerializer  

    def list(self, request, *args, **kwargs):
        response = super(MemberListCreate, self).list(request,*args, **kwargs)
        if response.status_code == 200:
            response.data = {'data': response.data}
        return response  

    def create(self, request, *args, **kwargs):
        data = request.data
        serializer = MemberSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        committee_ids = request.data['committee_ids'].split(',')
        response_data = {}
        try:
            with transaction.atomic():            
                member = serializer.save() 
                response_data.update({'id':member.id})               
                for id in committee_ids:
                    member.committees.create(id=int(id))
        except ValueError:
            return Response(status=400)        
        return Response(response_data, status=201)


class MemberUpdateDestroy(generics.UpdateAPIView, generics.DestroyAPIView):    
    serializer_class = MemberSerializer
    permission_classes = (IsAdminUser,)
    queryset = Member.objects.filter(is_active=True).filter(is_unregistered=False)
