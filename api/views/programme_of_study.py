from api.permissions import IsAdminOrReadOnly, IsAdminUser
from api.models import ProgrammeOfStudy
from api.serializers import ProgrammeOfStudySerializer

from rest_framework import generics


class ProgrammeOfStudyListCreate(generics.ListCreateAPIView):
    permission_classes = (IsAdminOrReadOnly,)
    queryset = ProgrammeOfStudy.objects.filter(is_active=True)
    serializer_class = ProgrammeOfStudySerializer

    def list(self, request, *args, **kwargs):
        response = super(ProgrammeOfStudyListCreate, self).list(request,*args, **kwargs)
        if response.status_code == 200:
            response.data = {'data': response.data}
        return response  


class ProgrammeOfStudyUpdateDestroy(generics.UpdateAPIView, generics.DestroyAPIView):
    permissions_classes = (IsAdminUser,)
    queryset = ProgrammeOfStudy.objects.filter(is_active=True)
    serializer_class = ProgrammeOfStudySerializer