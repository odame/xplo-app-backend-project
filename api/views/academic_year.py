from api.permissions import IsAdminOrReadOnly, IsAdminUser
from api.models import AcademicYear
from api.serializers import AcademicYearSerializer
from rest_framework import generics


class AcademicYearListCreate(generics.ListCreateAPIView):
    permission_classes = (IsAdminOrReadOnly,)
    queryset = AcademicYear.objects.all()
    serializer_class = AcademicYearSerializer


class AcademicYearUpdate(generics.UpdateAPIView):
    permission_classes = (IsAdminUser,)
    queryset= AcademicYear.objects.all()
    serializer_class = AcademicYearSerializer
