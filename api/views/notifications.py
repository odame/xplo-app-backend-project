from api.permissions import IsAdminOrReadOnly
from api.models import Notification, FCMClient
from api.serializers import NotificationSerializer, FCMClientSerializer
from api.utils import NotificationHandler
from django.db.models import Q
from django.utils.datetime_safe import datetime
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated, IsAdminUser
from rest_framework.response import Response


class NotificationListCreate(generics.ListCreateAPIView):
    serializer_class = NotificationSerializer
    queryset = Notification.objects.all()
    permission_classes = (IsAuthenticatedOrReadOnly, IsAdminOrReadOnly,)
    client_id = ''

    def filter_queryset(self, queryset):
        if self.request.method == 'GET':
            if not (self.request.user and self.request.user.is_staff):
                expiration_filter = Q(expiry_date__gte=datetime.today())
                client_unseen_notifications = \
                    (FCMClientNotification.objects.exclude(client=self.client_id).values_list('notification',
                                                                                              flat=True))
                client_filter = Q(client_notification__in=client_unseen_notifications)
                queryset = queryset.filter(expiration_filter & client_filter)
        return super(NotificationListCreate, self).filter_queryset(queryset)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        notification = serializer.save()
        NotificationHandler.send_notification(notification)
        return Response(data={'id': notification.id, }, status=status.HTTP_201_CREATED)

    def list(self, request, *args, **kwargs):
        self.client_id = kwargs['client_id']
        response = super(NotificationListCreate, self).list(request, *args, **kwargs) 
        if response.status_code == 200:
            response.data = {'data': response.data}
        return response       


class NotificationUpdateDestroy(generics.UpdateAPIView, generics.DestroyAPIView):
    serializer_class = NotificationSerializer
    queryset = Notification.objects.all()
    permission_classes = (IsAuthenticated, IsAdminUser,)


class FCMClientCreate(generics.CreateAPIView):
    serializer_class = FCMClientSerializer
    queryset = FCMClient.objects.all() 
    
    def create(self, request, *args, **kwargs):
        response = super(FCMClientCreate, self).create(request, *args, **kwargs)        
        if response.status_code == 201:
            # if successful, we replace the data of the response with just
            # an acknowledgement message
            # this choice is only intended to make the frontend development easier
            response.data = {'message': 'created successfully'}
        return response