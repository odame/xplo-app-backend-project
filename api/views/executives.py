from api.permissions import IsAdminOrReadOnly, IsAdminUser
from api.models import ExecutivePosition, ExecutiveMember, AcademicYear
from api.serializers import ExecutivePositionSerializer, ExecutiveMemberInfoSerializer, ExecutiveMemberSerializer

from rest_framework import generics


class ExecutivePositionListCreate(generics.ListCreateAPIView):
    permission_classes = (IsAdminOrReadOnly,)
    queryset = ExecutivePosition.objects.all()
    serializer_class = ExecutivePositionSerializer

    def list(self, request, *args, **kwargs):
        response = super(ExecutivePositionListCreate, self).list(request,*args, **kwargs)
        if response.status_code == 200:
            response.data = {'data': response.data}
        return response  


class ExecutivePositionUpdateDestroy(generics.UpdateAPIView, generics.DestroyAPIView):
    permission_classes = (IsAdminUser, )
    queryset = ExecutivePosition.objects.all()
    serializer_class = ExecutivePositionSerializer


class ExecutiveMemberListCreate(generics.ListCreateAPIView):
    permission_classes = (IsAdminOrReadOnly, )
    queryset = ExecutiveMember.objects.all()

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return ExecutiveMemberInfoSerializer
        else:
            return ExecutiveMemberSerializer

    def get_queryset(self):
        if self.request.method == 'GET':
            # for the grouping of executive members into academic years
            return AcademicYear.objects.all()
        else:
            # for creating new executive members
            return ExecutiveMember.objects.all()
    
    def list(self, request, *args, **kwargs):
        response = super(ExecutiveMemberListCreate, self).list(request,*args, **kwargs)
        if response.status_code == 200:
            response.data = {'data': response.data}
        return response  


class ExecutiveMemberUpdateDestroy(generics.UpdateAPIView, generics.DestroyAPIView):
    permission_classes = (IsAdminUser, )
    queryset = ExecutiveMember.objects.all()
    serializer_class = ExecutiveMemberSerializer
