from api.permissions import IsAdminOrReadOnly, IsAdminUser
from api.models import Section
from api.serializers import SectionSerializer

from rest_framework import generics


class SectionListCreate(generics.ListCreateAPIView):
    queryset = Section.objects.filter(is_active=True)
    permission_classes = (IsAdminOrReadOnly,)
    serializer_class = SectionSerializer  

    def list(self, request, *args, **kwargs):
        response = super(SectionListCreate, self).list(request,*args, **kwargs)
        if response.status_code == 200:
            response.data = {'data': response.data}
        return response  


class SectionUpdateDestroy(generics.UpdateAPIView, generics.DestroyAPIView):
    queryset = Section.objects.filter(is_active=True)
    permission_classes = (IsAdminUser,)
    serializer_class = SectionSerializer
