from api.permissions import IsAdminOrReadOnly, IsAdminUser
from api.models import Committee
from api.serializers import CommitteeSerializer
from XploAppBackend.utils import upload_image_to_imgur

from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError

from django.db.models import Count, Prefetch


class CommitteeListCreate(generics.ListCreateAPIView):
    permission_classes = (IsAdminOrReadOnly,)
    serializer_class = CommitteeSerializer    

    def get_queryset(self):
        queryset = Committee.objects.filter(is_active=True)
        if self.request.method == 'GET':
            queryset = queryset.annotate(number_of_members=Count('members'))
        return queryset
    
    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if not request.FILES or'file' not in request.FILES:
            raise ValidationError({'logo': ['Image file for logo not found']})
        # committee = Committee(serializer.validated_data)
        try:
            logo_url = upload_image_to_imgur(request.FILES['file'])
        except ValueError as e:
            raise ValidationError({'logo': [e.message]})
        data = {}
        data.update({'logo': logo_url})
        data.update(serializer.data)
        committee = Committee(**data)
        committee.is_active = True
        committee.save()
        serializer = self.get_serializer(committee)        
        headers = self.get_success_headers(serializer.data)
        return Response(data=serializer.data, status=status.HTTP_201_CREATED, headers=headers)
    
    def list(self, request, *args, **kwargs):
        response = super(CommitteeListCreate, self).list(request,*args, **kwargs)
        if response.status_code == 200:
            response.data = {'data': response.data}
        return response  



class CommitteeUpdateDestroy(generics.UpdateAPIView, generics.DestroyAPIView):
    permission_classes = (IsAdminUser, )
    queryset = Committee.objects.filter(is_active=True)
    serializer_class = CommitteeSerializer
