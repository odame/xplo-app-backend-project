from api.models import WelcomeScreenImages
from api.serializers import WelcomeScreenImagesSerializer
from api.permissions import IsAdminOrReadOnly, IsAdminUser

from XploAppBackend.utils import upload_image_to_imgur

from rest_framework import generics, status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response



class WelcomeScreenImagesListCreate(generics.ListCreateAPIView):
    permission_classes = (IsAdminOrReadOnly, )
    serializer_class = WelcomeScreenImagesSerializer
    queryset = WelcomeScreenImages.objects.all()   

    def create(self, request):
        if not request.FILES or 'file' not in request.FILES:
            raise ValidationError("Please specify the image file")
        try:
            image_url = upload_image_to_imgur(request.FILES['file'])
        except ValueError as e:
            raise ValidationError(e.message)
        welcome_screen_image = WelcomeScreenImages(image_url=image_url)
        welcome_screen_image.save()
        serializer = self.get_serializer(welcome_screen_image)
        return Response(data=serializer.data, status=status.HTTP_201_CREATED)

    def list(self, request, *args, **kwargs):
        response = super(WelcomeScreenImagesListCreate, self).list(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            response.data = {'data': response.data}
        return response


class WelcomeScreenImagesUpdateDestroy(generics.UpdateAPIView, generics.DestroyAPIView):
    permission_classes = (IsAdminUser,)
    queryset = WelcomeScreenImages.objects.all()   
    serializer_class = WelcomeScreenImagesSerializer   
