from api.serializers import MusicFileSerializer, MusicAlbumSerializer, MusicFileUploadSerializer
from api.permissions import IsAdminOrReadOnly
from api.models import MusicFile, MusicAlbum

from django.contrib.auth.models import User
from django.db import transaction
from rest_framework import generics, status
from rest_framework.exceptions import PermissionDenied
from rest_framework.parsers import FormParser
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response

from smartfile import BasicClient
import os
import urllib2
from pprint import pprint


class MusicAlbumListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = MusicAlbumSerializer
    # permission_classes = (IsAdminOrReadOnly,)
    queryset = MusicAlbum.objects.filter(is_active=True)


class MusicFileUploadAPIView(generics.CreateAPIView):    
    # permission_classes = (IsAdminOrReadOnly, )
    queryset = MusicFile.objects.all()
    parser_classes = (MultiPartParser,)

    def get_safe_filename(self, filename):
        for c in r'[]/\;,><&*:%=+@!#^()|?^':
            filename = filename.replace(c,'')
        return filename

    def create(self, request, *args, **kwargs):        
        serializer = MusicFileUploadSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)      
        smartfile_key = os.environ.get('SMARTFILE_API_KEY')
        smartfile_password = os.environ.get('SMARTFILE_API_PASSWORD')
        smartfile_api = BasicClient(key=smartfile_key, password=smartfile_password)
        fileobj = request.FILES['file']
        # check if the album exists
        album = None 
        music_obj = None
        response = None
        try:                
            with transaction.atomic():   
                try:
                    album = MusicAlbum.objects.get(title=request.data['album'])
                except MusicAlbum.DoesNotExist:
                    album_name = urllib2.quote(self.get_safe_filename(request.data['album']))
                    api_response = smartfile_api.put('/path/oper/mkdir/music_albums/' + album_name) #create the directory 
                    #create a public url for the directory                    
                    public_url_response = smartfile_api.post('/link/', path=urllib2.quote(api_response['path']), read=True, list=True)                    
                    album = MusicAlbum.objects.create(title=album_name, url=public_url_response['href'])
                finally:            
                    safe_album_name = urllib2.quote(self.get_safe_filename(request.data['album']))
                    safe_music_name = urllib2.quote(self.get_safe_filename(request.FILES['file'].name))
                    arg = (safe_music_name, fileobj)
                    smartfile_api_response = smartfile_api.post('/path/data/music_albums/' + safe_album_name, file=arg)
                    music_obj = MusicFile.objects.create(title=request.FILES['file'].name, album=album, url=album.url + safe_music_name + "?download=true")
        except:
            response = Response(data="Error while uploading file", status=400)
        else:
            serializer = MusicFileSerializer(music_obj)            
            response = Response(data=serializer.data, status=201)
        return response
