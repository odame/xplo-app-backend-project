from api.serializers import AdminSerializer
from django.contrib.auth.models import User
from rest_framework import generics, status
from rest_framework.exceptions import PermissionDenied
from rest_framework.parsers import FormParser
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response


class AdminListCreate(generics.ListCreateAPIView):    
    serializer_class = AdminSerializer
    permission_classes = (IsAuthenticated, IsAdminUser,)
    queryset = User.objects.filter(is_active=True).prefetch_related('profile')

    def create(self, request, *args, **kwargs):
        """
        creates and return a new member/user
        """
        response = super(AdminListCreate, self).create(request, *args, **kwargs)
        response.data = response.data['id']
        return response


class AdminUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, update or delete a member/user
    """    
    serializer_class = AdminSerializer
    queryset = User.objects.filter(is_active=True).prefetch_related('profile')
    permission_classes = (IsAuthenticated, IsAdminUser,)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.is_active = False
        instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        if request.user.username != self.get_object().username:
            raise PermissionDenied('You do not have permission to perform this action')
        return super(AdminUpdateDestroy, self).update(request, *args, **kwargs)
