from api.permissions import IsAdminOrReadOnly, IsAdminUser

from rest_framework import generics
from django.http import JsonResponse
from XploAppBackend.utils import upload_image_to_imgur

class UploadPhotoFile(generics.CreateAPIView):
    def create(self, request, *args, **kwargs):
        photo_url = upload_image_to_imgur(request.FILES['file'])
        response = JsonResponse({'url': photo_url})
        return response
