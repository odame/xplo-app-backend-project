from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import PermissionDenied


class Login(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        if not user.is_staff:
            raise PermissionDenied("You do not have permission to perform this operation")
        try:
            token = Token.objects.get(user=user)
            token.delete()
        except: pass
        token = Token.objects.create(user=user)
        return Response({'token': token.key})


class Logout(APIView):
    permission_classes = (IsAdminUser, )

    def post(self, request, *args, **kwargs):
        try:
            token = Token.objects.get(user=request.user)
            token.delete()
        except: pass
        return Response(status=status.HTTP_204_NO_CONTENT)
