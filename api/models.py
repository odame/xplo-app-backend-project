﻿"""
Definition of models.
"""

from django.db import models
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.core.validators import MaxValueValidator, MinValueValidator

from rest_framework.authtoken.models import Token

import datetime
import uuid

# Storage backend for serving static files
# google_drive_storage = GoogleDriveStorage()  


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def generate_token(sender, instance, create=False, **kwargs):
    """
    automatically generate token for new admin
    """
    if instance.is_staff:
        Token.objects.get_or_create(user=instance)


class Section(models.Model):
    """
    Hostels are grouped into sections
    """
    name = models.CharField(unique=True, max_length=30)
    date_added = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def delete(self):
        self.is_active = False
        self.save()


class Hostel(models.Model):
    """
    The place of residence of member
    """
    name = models.CharField(unique=True, max_length=30)
    section = models.ForeignKey(Section, on_delete=models.PROTECT, related_name='hostels')
    is_active = models.BooleanField(default=True)

    def delete(self):
        self.is_active = False
        self.save()


class Member(models.Model):
    """
    Information about members
    """ 
    gender = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    levels = (
        ('100', 'Level 100'),
        ('200', 'Level 200'),
        ('300', 'Level 300'),
        ('400', 'Level 400'),
        ('500', 'Level 500'),
        ('600', 'Level 600'),
    )    
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    other_names = models.CharField(max_length=100, blank=True, null=True)
    email = models.EmailField(max_length=254, blank=True, null=True)
    photo = models.CharField(max_length=40, blank=True, null=True)
    date_of_birth = models.DateField()
    phone1 = models.CharField(max_length=25, blank=True, null=True)
    phone2 = models.CharField(max_length=25, blank=True, null=True)
    room_number = models.CharField(max_length=20)
    year_of_study = models.CharField(max_length=3, choices=levels)
    programme_of_study = models.ForeignKey('ProgrammeOfStudy', related_name='members')
    hostel = models.ForeignKey('Hostel', on_delete=models.PROTECT, related_name='members')
    gender = models.CharField(max_length=1, choices=gender)
    date_registered = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)  # indicates the member is just currently not active. For example is the member is now an alumni
    is_unregistered = models.BooleanField(default=False)  # indicates the member is 'deleted'
    is_confirmed = models.BooleanField(default=False)   # indicated the member has not yet been confirmed by an admin
    committees = models.ManyToManyField('Committee',  related_name='members')

    def delete(self):
        self.is_unregistered = True
        self.save()
    
    def deactivate(self):
        self.is_active = False
        self.save()

    def is_executive(self):
        return False

    def full_name(self):
        return self.first_name + (' ' if not self.other_names else ' {} '.format(self.other_names)) + self.last_name


class Committee(models.Model):
    name = models.CharField(max_length=30, unique=True)
    date_added = models.DateField(auto_now_add=True)
    info = models.CharField(max_length=500, blank=True, null=True)
    logo = models.CharField(max_length=40, blank=True, null=True)
    is_active = models.BooleanField(default=True)

    def delete(self):
        self.is_active = False
        self.save()


class AcademicYear(models.Model):
    start_year = models.IntegerField(validators=[
        MinValueValidator(2000),
        MaxValueValidator(9999),
    ])
    end_year = models.IntegerField(validators=[
        MinValueValidator(2000),
        MaxValueValidator(9999),
    ])    
    
    def get_year_str(self):        
        return str(self.start_year) + ' / ' + str(self.end_year)

    class Meta:
        ordering = ['-end_year',]
        unique_together = (('start_year', 'end_year'), )


class ProgrammeOfStudy(models.Model):
    name = models.CharField(max_length=50)
    is_active = models.BooleanField(default=True)


class ExecutivePosition(models.Model):
    """
    A model of the executive positions
    """
    position_name = models.CharField(max_length=30, unique=True)
    date_added = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def delete(self):
        self.is_active = False
        self.save()


class ExecutiveMember(models.Model):
    """
    The executives over the years, including current ones
    """
    member = models.ForeignKey('Member', on_delete=models.PROTECT, related_name='executives')
    position = models.ForeignKey('ExecutivePosition', on_delete=models.PROTECT)
    academic_year = models.ForeignKey('AcademicYear', on_delete=models.PROTECT, related_name='executive_members')
    class Meta:
        unique_together = (('position', 'academic_year'),)


class FCMClient(models.Model):
    client_id = models.TextField(primary_key=True)    
    notifications = models.ManyToManyField('Notification')


class Notification(models.Model):
    """
    Notification that will be sent to users
    """
    message = models.CharField(max_length=300)
    date_created = models.DateTimeField(auto_now_add=True)
    date_sent = models.DateTimeField(blank=True, null=True)
    expiry_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ['-date_sent']


class WelcomeScreenImages(models.Model):
    image_url = models.CharField(max_length=40)


class MusicFile(models.Model):
    title = models.CharField(max_length=50)
    url = models.TextField()
    date_uploaded = models.DateTimeField(auto_now_add=True)
    album = models.ForeignKey("MusicAlbum", related_name="music_files", on_delete=models.PROTECT)
    is_active = models.BooleanField(default=True)


class MusicAlbum(models.Model):
    title = models.CharField(max_length=50, primary_key=True)
    date_created = models.DateTimeField(auto_now_add=True)
    url = models.TextField()
    is_active = models.BooleanField(default=True)
