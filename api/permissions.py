﻿from rest_framework.permissions import *


class IsAdminOrReadOnly(BasePermission):
    """
    permission is allowed if method is safe, otherwise, user must be an admin
    """
    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        is_admin = request.user and request.user.is_authenticated and request.user.is_staff
        return (request.method in SAFE_METHODS) or is_admin
   