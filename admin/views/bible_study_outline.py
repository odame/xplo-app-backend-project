from api.models import *

from django.views import View
from django.shortcuts import render


class BibleStudyOutlineView(View):
    template_context = {'page_title': 'Bible Study Outline'}
    template = 'base.html'

    def get(self, request):
        return render(request, self.template, content_type=self.template_context)