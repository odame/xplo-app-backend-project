# from api.models import *
from api.models import Member, Committee, Hostel, ProgrammeOfStudy
from admin.template_filters import full_name
from api.serializers import MemberSerializer

from django.views import View
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.db.models import Prefetch, Count
from rest_framework.serializers import ValidationError

from datetime import datetime
from random import choice


class MembersView(View):
    template_context = {}
    template = 'members.html'

    def get(self, request):
        page_title_format_args = {}
        members = []  # the list of members to be displayed
        queryset = Member.objects.filter(is_confirmed=True,
            is_unregistered=False).order_by('first_name', 'last_name')

        # Select members in the appropriate time period.
        # By default, we return only the currently active members.
        # The values for the 'period' argument are 'c', 'p', 'a' for 'current',
        # 'past' and 'all' respectively
        period = 'c' if not 'period' in request.GET else request.GET['period']
        if period == 'c':
            queryset = queryset.filter(is_active=True)
            page_title_format_args.update({'period': 'Current'})
        elif period == 'p':
            queryset = queryset.filter(is_active=False)
            page_title_format_args.update({'period': 'Past'})
        else:
            page_title_format_args.update({'period': 'All'})
            # for any other value, we return all members, both present and past
        self.template_context.update({'period': period})

        # attach any related models of members in queryset if applicable

        # select members for the appropriate category
        if 'committee' in request.GET:
            try:
                committee = Committee.objects.filter(is_active=True). \
                    prefetch_related(Prefetch(
                        'members',
                        queryset=queryset
                    )).get(id=request.GET['committee'])
                members = list(committee.members.all())
                committee_name_title = committee.name if 'committee' in committee.name else '{} Committee'.format(
                    committee.name)
                page_title_format_args.update(
                    {'extra': ": " + committee_name_title})
            except Committee.DoesNotExist:
                page_title_format_args.update({'extra': ''})
                # if the committee does not exist, we just return an empty list
                # to the user
        elif 'section' in request.GET:
            section = Section.objects.filter(is_active=True). \
                prefetch_related(Prefetch(
                    'hostels',
                    queryset=Hostel.objects.filter(is_active=True).prefetch_related(Prefetch(
                        'members',
                        queryset=queryset
                    )),
                )).get(id=request.GET['section'])
            for hostel in section.hostels.all():
                members.extend(list(hostel.members.all()))
            page_title_format_args.update(
                {'extra': ": " + section.name + ' section'})
        elif 'programme-of-study' in request.GET:
            try:
                programme = ProgrammeOfStudy.objects.filter(is_active=True). \
                    prefetch_related(Prefetch(
                        'members',
                        queryset=queryset
                    )).get(id=request.GET['programme-of-study'])
                members = list(programme.members.all())
                page_title_format_args.update({'extra': ": " + programme.name})
            except ProgrammeOfStudy.DoesNotExist:
                page_title_format_args.update({'extra': ''})
        elif 'hostel' in request.GET:
            try:
                hostel = Hostel.objects.filter(is_active=True). \
                    prefetch_related(Prefetch(
                        'members',
                        queryset=queryset
                    )).get(id=request.GET['hostel'])
                members = list(hostel.members.all())
                page_title_format_args.update({'extra': ": " + hostel.name})
            except Hostel.DoesNotExist:
                page_title_format_args.update({'extra': ''})
        else:
            page_title_format_args.update({'extra': ''})
            members = list(queryset)

        self.template_context.update({
            'members': members,
            'page_title': '{period} Members {extra}'.format(**page_title_format_args),
        })
        return render(request, self.template, context=self.template_context)


class MemberDetailView(View):
    template = 'member_detail.html'
    template_context = {'page_title': "Member's Detail"}

    def get(self, request, *args, **kwargs):
        try:
            member = Member.objects.filter(is_unregistered=False, is_active=True, is_confirmed=True).prefetch_related(
                'committees').prefetch_related('programme_of_study'). \
                prefetch_related(Prefetch('hostel', queryset=Hostel.objects.all(
                ).prefetch_related('section'))).get(id=kwargs['id'])
            setattr(member, 'all_committees', member.committees.all())
            self.template_context.update({
                'member': member,
                'hostels': Hostel.objects.filter(is_active=True),
                'programmes_of_study': ProgrammeOfStudy.objects.filter(is_active=True)
            })
        except Member.DoesNotExist:
            pass
        return render(request, self.template, context=self.template_context)

    def update(self, request, *args, **kwargs):
        post_data = {'id': kwargs['id']}.update(request.POST)
        # TODO: Use a django form rather
        serializer = MemberSerializer(data=post_data)
        try:
            if serializer.is_valid():
                updated_member = serializer.save()
                if 'committees' in request.POST:
                    committee_ids = request.POST['committees'].split(",")
                    if len(committee_ids) == 0:
                        raise ValidationError(
                            "Member must belong to a committee")
                    member = serializer.save()
                    member_committees = []
                    for com in committees:
                        if com.id in committee_ids:
                            member_committees.append(com)
                    member.committees.add(*member_committees)
                    self.template_context.update(
                        {'success_message': "Member has been registered successfully"})
            else:
                raise ValidationError("Error Parsing Data")
        except ValidationError as e:
            self.template_context.update({'error_message': e.message})
            self.template_context.update(request.POST)
        self.template_context.update({
            'committees': committees,
            'hostels': hostels,
            'programmes_of_study': programmes_of_study,
        })
        return render(request, self.template, context=self.template_context)
        # get the member we want to edit/update
        # try:
        #     member = Member.objects.filter(
        #         is_unregistered=False).get(id=kwargs['id'])
        # except Member.DoesNotExist:
        #     # if the member does not exist. This can't happen looking at the
        #     # current scenarios governing the app
        #     self.template_context.update(
        #         {'error_message': 'Member was not found'})


class MemberDeleteView(View):

    def get(self, request, *args, **kwargs):
        try:
            member = Member.objects.get(id=kwargs['id'])
            if kwargs['action'] == 'unreg':
                member.is_unregistered = True
            member.is_active = False
            member.save()
        except Member.DoesNotExist:
            pass
        return redirect('admin-members')


class NewMemberView(View):
    template_context = {'page_title': 'Register new member'}
    template = 'new_member.html'

    def get(self, request):
        self.template_context.update({
            'committees': list(Committee.objects.filter(is_active=True).order_by('name')),
            'hostels': list(Hostel.objects.filter(is_active=True).order_by('name')),
            'programmes_of_study': list(ProgrammeOfStudy.objects.filter(is_active=True).order_by('name')),
        })
        return render(request, self.template, context=self.template_context)

    def post(self, request):
        # data that will be sent back to the client anyways
        committees = list(Committee.objects.filter(
            is_active=True).order_by('name'))
        hostels = list(Hostel.objects.filter(is_active=True).order_by('name'))
        programmes_of_study = list(
            ProgrammeOfStudy.objects.filter(is_active=True).order_by('name'))
        status = 201

        # TODO: Use a django form rather
        serializer = MemberSerializer(data=request.POST)
        try:
            if serializer.is_valid() and 'committees' in request.POST:
                committee_ids = request.POST['committees'].split(",")
                if len(committee_ids) == 0:
                    raise ValidationError("Member must belong to a committee")
                member = serializer.save()
                member_committees = []
                for com in committees:
                    if com.id in committee_ids:
                        member_committees.append(com)
                member.committees.add(*member_committees)
                self.template_context.update(
                    {'success_message': "Member has been registered successfully"})
            else:
                raise ValidationError("Error Parsing Data")
        except ValidationError as e:
            status = 400
            self.template_context.update({'error_message': e.message})
            # self.template_context.update(request.POST)
        self.template_context.update({
            'committees': committees,
            'hostels': hostels,
            'programmes_of_study': programmes_of_study,
        })
        return render(request, self.template, context=self.template_context, status=status)


def add_members(request):
    names = [
        'Odame', 'Prince', 'Samuel', 'Kwabena', 'Yaw', 'Kwame', 'Kwasi', 'Frank',
        'Justice', 'Obed', 'Sagoe', 'Emmanuel', 'Hun', 'Daniel', 'Ben'
    ]
    section_ids = Section.objects.values_list('id', flat=True)
    hostel_ids = Hostel.objects.values_list('id', flat=True)
    pg_ids = ProgrammeOfStudy.objects.values_list('id', flat=True)
    levels = ['100', '200', '300', '400', '500', '600']
    for i in range(10):
        member_info = {
            'last_name': choice(names),
            'first_name': choice(names),
            'other_names': choice(names),
            'email': 'opodame@gmail.com',
            'date_of_birth': datetime.today(),
            'room_number': '345r',
            'year_of_study': choice(levels),
            'programme_of_study_id': choice(pg_ids),
            'hostel_id': choice(hostel_ids),
            'gender': 'M'
        }
        member = Member(**member_info)
        member.save()
    return HttpResponse("test members created successfully", status=201)


class UnConfirmedMembersView(View):
    '''
    Confirm new members' registration
    '''
    template_context = {'page_title': "Confirm members' registration"}
    template = 'unconfirmed_members.html'

    def get(self, request, id=None):
        '''
        Get the data for all unconfirmed members or a single unconfirmed member
        '''
        if(id):
            # we retrieve the data for only a single unconfirmed user
            self.template = 'unconfirmed_member_detail.html'
            try:
                member = Member.objects.filter(
                    is_unregistered=False, is_active=True, is_confirmed=False
                ).prefetch_related('committees').prefetch_related('programme_of_study'). \
                    prefetch_related(
                        Prefetch(
                            'hostel', queryset=Hostel.objects.all().prefetch_related('section')
                        )
                    ).get(id=id)
                setattr(member, 'all_committees', member.committees.all())
                self.template_context.update({
                    'member': member,
                    'committees': Committee.objects.filter(is_active=True),
                    'hostels': Hostel.objects.filter(is_active=True),
                    'programmes_of_study': ProgrammeOfStudy.objects.filter(is_active=True)
                })
            except Member.DoesNotExist:
                pass
        else:
            # we retrieve the data for all unconfirmed members
            self.template_context.update({
                'members': list(
                    Member.objects.filter(is_active=True, is_confirmed=False)
                ),
            })
        return render(request, self.template, context=self.template_context)

    def post(self, request, action, id=None):
        '''
        Accept/Reject the registration of users
        '''
        confirmed = action == 'accept'
        if(id):
            try:            
                member = Member.objects.get(id=id)
                if not confirmed:
                    member.delete()
                else:
                    member.ic_confirmed = confirmed                
                    member.save()
            except Member.DoesNotExist:
                pass
        else:
            unconfirmed_members = Member.objects.filter(is_confirmed=False)
            if not confirmed:
                unconfirmed_members.delete()
            else:
                unconfirmed_members.update(is_confirmed=True)
        return redirect('admin-unconfirmed_members')
