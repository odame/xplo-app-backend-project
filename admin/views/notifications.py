from api.models import Notification, FCMClient
from api.serializers import NotificationSerializer

from django.views import View
from django.shortcuts import render

from pyfcm import FCMNotification
from datetime import datetime

from os import environ
import time
import logging
from pprint import pprint


class NotificationsView(View):
    template_context = {'page_title': 'Notifications'}
    template = 'notifications.html'

    def get(self, request):
        self.template_context.update({
            'notifications': Notification.objects.order_by("-date_created").all()
        })
        return render(request, self.template, context=self.template_context)

    def post(self, request):
        serializer = NotificationSerializer(data=request.POST)
        if serializer.is_valid():
            notification = serializer.save()
            api_key = environ.get("XPLO_FIREBASE_CLOUD_MESSAGING_API_KEY","NO-KEY-WAS-RECEIVED-FROM-ENVIRON")                    
            push_service = FCMNotification(api_key=api_key)
            payload = {
                # 'click_action': "com.android.xploapp.gospelexplosion_knust.NEW_NOTIFICATION_ACTION", # the name of the intent that will be attached when users tap the notification
                'date': notification.date_created.strftime("%Y-%m-%d %H:%M %p"),
                'timestamp':int(time.time()),
                'message': notification.message,
                'id': notification.id
            }                       
            # pprint(result, indent=4)
            result = push_service.notify_topic_subscribers(topic_name="general_notifications", message_title="Xplo KNUST", message_body=None, data_message=payload, time_to_live=3600*24*7)
            if result['success'] > 0:
                self.template_context.update({'success_message':"Notification was sent successfully"}) 
            else:
                self.template_context.update({'erorr_message':"Notification was not sent"}) 
        else:
            self.template_context.update({'erorr_message':"Notification was not sent"})
        self.template_context.update({
            'notifications': Notification.objects.order_by("-date_created").all()
        })        
        return render(request, self.template, context=self.template_context)
