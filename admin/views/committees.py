from api.models import *

from django.shortcuts import render, redirect
from django.views import View
from django.db.models import Count, Prefetch


class CommitteesView(View):
    template_context = {'page_title': 'Committees'}
    template = 'committees.html'

    def get(self, request):
        committees = list(
            Committee.objects.filter(is_active=True).prefetch_related(
                Prefetch(
                    'members',
                    queryset=Member.objects.filter(is_active=True).filter(is_unregistered=False),
                    to_attr='active_members'
                )
            )
        )
        for committee in committees:
            setattr(committee, 'number_of_members', len(committee.active_members))        
        self.template_context.update({
            'committees': committees,
        })

        # check if a message was added to session from a POST previously
        msg_key, msg_value = (None, None)
        if request.session.get('new_committee_message') is not None:
            if request.session.get('new_committee_message') == 1:
                msg_key, msg_value = ('success_message', 'Added successfully')
            elif request.session.get('new_committee_message') == 0:
                msg_key, msg_value = ('error_message', 'Could not add committee')
            del request.session['new_committee_message']    # remove the value from session  
        self.template_context.update({msg_key:msg_value})

        return render(request, self.template, context=self.template_context)
    
    def post(self, request):
        committee_name = request.POST.get('name')
        info = request.POST.get("info", "")
        if committee_name is not None:
            try:
                committee = Committee(name=committee_name, info=info)
                committee.save()
                request.session['new_committee_message'] = 1
            except:
                # another committee with the same name exist perhaps
                try:
                    committee = Committee.objects.get(name=committee_name)
                    committee.is_active = True
                    committee.save()
                    request.session['new_committee_message'] = 1
                except Committee.DoesNotExist:
                    request.session['new_committee_message'] = 0        
        else:
            request.session['new_committee_message'] = 0
        return redirect('admin-committees')


class CommiteeDetailView(View):
    template_context = {'page_title': 'Committee Detail'}
    template = 'committee_detail.html'
    
    def get(self, request, *args, **kwargs):
        committee = Committee.objects.get(id=kwargs['id'])
        self.template_context.update({
            'committee': committee,
        })
        return render(request, self.template, context=self.template_context)
