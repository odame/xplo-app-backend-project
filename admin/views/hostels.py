from django.shortcuts import render, redirect
from django.views import View
from django.db.models import Prefetch
from django.http import HttpResponse, HttpResponseNotFound, QueryDict
from django.db import IntegrityError

from api.models import Hostel, Member, Section


class HostelsView(View):
    template_name = 'hostels.html'
    template_context = {'page_title': "Hostels"}
    def get(self, request):
        sections = Section.objects.filter(is_active=True)
        hostels = Hostel.objects.filter(is_active=True).prefetch_related(
            Prefetch(
                'members',
                queryset=Member.objects.filter(is_active=True).filter(is_unregistered=False),
                to_attr = 'current_members'                            
            )
        ).order_by('name')
        for hs in hostels:
            setattr(hs, 'number_of_members', len(hs.current_members))      
        self.template_context.update({
            'hostels': hostels,
            'sections': sections
        })

        # check if a message was added to session from a POST previously
        msg_key, msg_value = (None, None)
        if request.session.get('new_hostel_message') is not None:
            if request.session.get('new_hostel_message') == 1:
                msg_key, msg_value = ('success_message', 'Added successfully')
            elif request.session.get('new_hostel_message') == 0:
                msg_key, msg_value = ('error_message', 'Could not add hostel')
            del request.session['new_hostel_message']    # remove the value from session  
        self.template_context.update({msg_key:msg_value})
        
        return render(request, self.template_name, context=self.template_context)

    def post(self, request):
        # create and save the new Hostel and add any appropriate message
        hostel_name = request.POST.get('name')  
        section_id = request.POST.get('section')
        has_errors = section_id is None or hostel_name is None      
        if not has_errors:
            hostel_name = hostel_name.strip().title()
            try:
                hostel = Hostel(name=hostel_name, section_id=section_id)
                hostel.save()  
                request.session['new_hostel_message'] = 1                
            except IntegrityError:
                #   an error means the hostel already exist, perhaps inactive 
                #   so we just find that hostel and make it active
                try:
                    hostel = Hostel.objects.get(name=hostel_name)
                    hostel.is_active = True
                    hostel.save()
                    request.session['new_hostel_message'] = 1
                except Hostel.DoesNotExist:
                    request.session['new_hostel_message'] = 0   
        else:
            request.session['new_hostel_message'] = 0   
        return redirect('admin-hostels')


class HostelUpdateView(View):
    def delete(self, request, *args, **kwargs):
        hostel_id = kwargs['id']
        try:
            hostel = Hostel.objects.get(id=hostel_id)
            hostel.is_active=False
            hostel.save()
        except Hostel.DoesNotExist:
            return HttpResponseNotFound(content='hostel was not found')
        return HttpResponse(content='Deletion was successful', status=203)

    def post(self, request, *args, **kwargs):                
        new_name = request.POST.get('name')
        if new_name is not None and len(new_name) != 0:
            hostel = Hostel.objects.filter(is_active=True).get(id=kwargs['id'])
            hostel.name = new_name
            hostel.save()
        return redirect('admin-hostels')  # redirect to the list of hostels