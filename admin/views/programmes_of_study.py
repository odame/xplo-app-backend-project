from django.shortcuts import render, redirect
from django.views import View
from django.db.models import Prefetch
from django.http import HttpResponse, HttpResponseNotFound,QueryDict

from api.models import ProgrammeOfStudy, Member


class ProgrammesOfStudyView(View):
    template_name = 'programmes_of_study.html'
    template_context = {'page_title': "Programmes of Study"}
    def get(self, request):
        programmes = ProgrammeOfStudy.objects.filter(is_active=True).prefetch_related(
            Prefetch(
                'members',
                queryset=Member.objects.filter(is_active=True).filter(is_unregistered=False),
                to_attr = 'current_members'                            
            )
        ).order_by('name')
        for pg in programmes:
            setattr(pg, 'number_of_members', len(pg.current_members))      
        self.template_context.update({
            'programmes':programmes
        })        
        # check if a message was added to session from a POST previously
        msg_key, msg_value = (None, None)
        if request.session.get('new_programme_message') is not None:
            if request.session.get('new_programme_message') == 1:
                msg_key, msg_value = ('success_message', 'Added successfully')
            elif request.session.get('new_programme_message') == 0:
                msg_key, msg_value = ('error_message', 'Could not add programme')
            del request.session['new_programme_message']    # remove the value from session  
        self.template_context.update({msg_key:msg_value})
                          
        return render(request, self.template_name, context=self.template_context)

    def post(self, request):
        # create and save the new ProgrammeOfStudy and add appropriate message
        programme_name = request.POST.get('name')
        if programme_name is not None:
            programme = ProgrammeOfStudy(name=programme_name.title())
            programme.save()  
            request.session['new_programme_message'] = 1           
        else:
            request.session['new_programme_message'] = 0                                  
        return redirect('admin-programmes_of_study')


class ProgrammeOfStudyUpdateView(View):
    def delete(self, request, *args, **kwargs):
        programme_id = kwargs['id']
        try:
            programme = ProgrammeOfStudy.objects.get(id=programme_id)
            programme.is_active=False
            programme.save()
        except ProgrammeOfStudy.DoesNotExist:
            return HttpResponseNotFound(content='programme was not found')
        return HttpResponse(content='Deletion was successful', status=203)  

    def post(self, request, *args, **kwargs):                
        new_name = request.POST.get('name')
        if new_name is not None and len(new_name) != 0:
            programme = ProgrammeOfStudy.objects.filter(is_active=True).get(id=kwargs['id'])
            programme.name = new_name
            programme.save()
        return redirect('admin-programmes_of_study')  # redirect to the list of programmes