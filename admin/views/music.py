from api.models import *

from django.views import View
from django.shortcuts import render


class MusicRoomView(View):
    template_context = {'page_title': 'Music Room'}
    template = 'base.html'

    def get(self, request):
        return render(request, self.template, context=self.template_context)