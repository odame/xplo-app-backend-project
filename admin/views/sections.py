from api.models import *

from django.shortcuts import render
from django.views import View
from django.db.models import Count, Prefetch


class SectionsView(View):
    template_context = {'page_title': 'Sections'}
    template = 'sections.html'

    def get(self, request):
        sections = list(
            Section.objects.filter(is_active=True).prefetch_related(
                Prefetch(
                        'hostels',
                        queryset=Hostel.objects.filter(is_active=True).prefetch_related(
                            Prefetch(
                                'members',
                                queryset=Member.objects.filter(is_active=True).filter(is_unregistered=False),
                                to_attr='active_members'
                            )
                        ),
                        to_attr='active_hostels'
                )
            )
        )
        # get the active hostels and active mebers of each of the sections
        for section in sections:            
            setattr(section, 'number_of_hostels', len(section.active_hostels))
            members_count = 0
            for hostel in section.active_hostels:
                members_count += len(hostel.active_members)
            setattr(section, 'number_of_members', members_count)

        self.template_context.update({
            'sections': sections,
        })
        return render(request, self.template, context=self.template_context)


class SectionDetailView(View):
    template_context = {'page_title': 'Section Detail'}
    template = 'section_detail.html'
    
    def get(self, request, *args, **kwargs):
        section = Section.objects.get(id=kwargs['id'])
        self.template_context.update({
            'section': section,
        })
        return render(request, self.template, context=self.template_context)
