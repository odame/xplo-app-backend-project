from django.conf.urls import url

from views.members import MembersView, MemberDetailView, NewMemberView, MemberDeleteView, UnConfirmedMembersView, add_members
from views.home import HomeView
from views.committees import CommiteeDetailView, CommitteesView
from views.hostels import HostelsView, HostelUpdateView
from views.sections import SectionDetailView, SectionsView
from views.programmes_of_study import ProgrammesOfStudyView, ProgrammeOfStudyUpdateView
from views.bible_study_outline import BibleStudyOutlineView
from views.gallery import GalleryView
from views.notifications import NotificationsView
from views.music import MusicRoomView


urlpatterns = [
    url(r'^$', HomeView.as_view(), name='admin-home'),


    url(r'^members/$', MembersView.as_view(), name='admin-members'),
    url(r'^members/(?P<id>[0-9]+)/$',
        MemberDetailView.as_view(), name='admin-member_detail'),
    url(r'^members/(?P<id>[0-9]+)/del/(?P<action>(inact|unreg))/$',
        MemberDeleteView.as_view(), name='admin-member_delete'),
    url(r'^members/new/$', NewMemberView.as_view(), name='admin-new_member'),


    url(r'^unconfirmed-members/$', UnConfirmedMembersView.as_view(),
        name='admin-unconfirmed_members'),
    url(r'^unconfirmed-members/(?P<action>(reject|accept))/$',
        UnConfirmedMembersView.as_view(), name='admin-unconfirmed_members'),
    url(r'^unconfirmed-members/(?P<id>[0-9]+)/$',
        UnConfirmedMembersView.as_view(), name='admin-unconfirmed_members'),
    url(r'^unconfirmed-members/(?P<id>[0-9]+)/(?P<action>(reject|accept))/$',
        UnConfirmedMembersView.as_view(), name='admin-unconfirmed_members'),


    url(r'^random-members/$', add_members),


    url(r'^committees/$', CommitteesView.as_view(), name='admin-committees'),
    url(r'^committees/(?P<id>[0-9]+)/$',
        CommiteeDetailView.as_view(), name='admin-committee_detail'),


    url(r'^hostels/$', HostelsView.as_view(), name='admin-hostels'),
    url(r'^hostels/(?P<id>[0-9]+)/$',
        HostelUpdateView.as_view(), name='admin-hostel_update'),


    url(r'^sections/$', SectionsView.as_view(), name='admin-sections'),
    url(r'^sections/(?P<id>[0-9]+)/$',
        SectionDetailView.as_view(), name='admin-section_detail'),


    url(r'^programmes-of-study/$', ProgrammesOfStudyView.as_view(),
        name='admin-programmes_of_study'),
    url(r'^programmes-of-study/(?P<id>[0-9]+)/$', ProgrammeOfStudyUpdateView.as_view(
    ), name='admin-programme_of_study_update'),


    url(r'^bible-study-outline/$', BibleStudyOutlineView.as_view(),
        name='admin-bible_study_outline'),


    url(r'^music-room/$', MusicRoomView.as_view(), name='admin-music_room'),


    url(r'^gallery/$', MusicRoomView.as_view(), name='admin-gallery'),


    url(r'^notifications/$', NotificationsView.as_view(),
        name='admin-notifications'),
]
