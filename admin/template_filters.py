from django.template import Library
register = Library()


@register.filter(is_safe=True)
def full_name(member):
    return member.full_name()
